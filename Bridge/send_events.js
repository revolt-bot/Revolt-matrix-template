const { get_handler } = require("../message_events");
const Revolt = require("revolt.js");

const md=require("markdown-it");

var handler= get_handler();



async function revolt_send_masq_message(content,channel, username, pfp_link, attachments,reply_id=null){

	
	let ch;
		
	if(typeof(channel)=="string"){
		ch= await handler.R.get_room(channel);
	} else{
		ch=channel;
	}
	
	if(!!reply_id)
		for(i in reply_id){
			reply_id[i]={
				id:reply_id[i],
				mention:true
			}
			
		}
	
	
	
	
	let m =ch.sendMessage({
			content: content,
			replies: reply_id,
			masquerade: {
				name: username,
				avatar: pfp_link,
			}
		}).catch((err) => {	console.log(err)});
	
	
	return m;
}




async function matrix_send_masq_message(content,channel, username, pfp_link, attachments,reply_id=null){
	
	let bodyy="**"+username+"**:\n >"+content;
	let mark=md();
	let M_format=mark.render(bodyy);
	
	let message={
		"body": bodyy,
		"format": "org.matrix.custom.html",
		"formatted_body": M_format,
		"msgtype": "m.text"
	};
	
	
	if(!!reply_id){
		message["m.relates_to"]={
			"m.in_reply_to":{
				"event_id":reply_id[0]
			}
		};		
	}
	
	
	
	
	let m=await handler.M.send_message(message,channel);
	

	
	return m;
}


async function	revolt_send_image_url_masq(message,url,channel_id,username,pfp_link,filename="a.png"){
		
		if(!handler.R)
			handler=get_handler(null);
		if(!handler.R)
			return;
		let channel_si= handler.R.get_room(channel_id);
		uploader=handler.R.uploader;
		
		
		Promise.allSettled([
				uploader.uploadUrl(url,filename),
			  	]).then(attachments => { // we're using Promise.allSettled to asynchronously upload all of them
			    	attachments = attachments.map(attachment => attachment.value); // extracting the value from the promises
					
			    	// send the attachment to the channel
			    	channel_si.sendMessage({
						content: ">",
						masquerade: {
							name: username,
							avatar: pfp_link,
						},
						attachments: attachments
		    	});
   			 // All done!
 		});
}



async function	matrix_send_image_url_masq(message,url,channel_id,username,pfp_link,filename="a"){
	//I will fix the matrix side, soon, but I dont have a appservice account yet.	
	
	handler.M.send_message(username+" sent a file:",channel_id);
	handler.M.send_image_url(url,channel_id);
}




async function send_masq_message(place,content,channel, username, pfp_link, attachments,reply_id=null){
	
	let mes;
	
	switch(place){
		case 'R':
			mes=await revolt_send_masq_message(content,channel,username,pfp_link,attachments,reply_id);

			mes=mes.id;
			break;
		
		case 'M':
			mes= await matrix_send_masq_message(content,channel, username, pfp_link, attachments,reply_id);
			break;
		default:
			break;
	}
	
	
	let str=place+"--"+channel+"--"+mes;
	
	return str;

};


//Ill edit this function later
//iLL have it look into the db
function parse_channel(channel_id){
	
	if(channel_id.includes(":"))
		return 'M';
	
	return 'R';
}



async function send_masq_image(place,url,channel_id,username,pfp_link,filename="a"){
	switch(place){
		case 'R':
			revolt_send_image_url_masq(place,url,channel_id,username,pfp_link,filename);
			break;
		case 'M':
			matrix_send_image_url_masq(place,url,channel_id,username,pfp_link,filename);
			break;
	}
}

module.exports={send_masq_image,send_masq_message,parse_channel}