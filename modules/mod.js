
const { Command } = require("../commands");


function _kick(args,handler,event,prefix){
	if(!args[1]){
		handler.reply(event,"Wrong usage, do "+args[0]+" UserID");
	}
	console.log("kick called");
	handler.kick(event,args[1]).then(res=>{
		
	console.log("kick_admin.js");
	if(res<0)
		handler.reply(event,"Error, the bot doesnt have permission to kick");
		
	})
}

function _ban(args,handler,event,prefix){
	if(!args[1]){
		handler.reply(event,"Wrong usage, do "+args[0]+" UserID");
	}
	console.log("kick called");
	handler.ban(event,args[1]).then(res=>{
		
	console.log("kick_admin.js");
	if(res<0)
		handler.reply(event,"Error, the bot doesnt have permission to kick");
		
	})
}


function _echo(args,handler,event,prefix){
	event.reply(args[1]);
	
}


const echo = new Command("echo",(args,handler,event,prefix) => {return _echo(args,handler,event,prefix);},null); //null means it requires no permission



const kick = new Command("kick",(args,handler,event,prefix) => {return _kick(args,handler,event,prefix);},{permissions: ["KickMembers"]});


const ban = new Command("ban",(args,handler,event,prefix) => {return _ban(args,handler,event,prefix);},{permissions: ["BanMembers"],});

